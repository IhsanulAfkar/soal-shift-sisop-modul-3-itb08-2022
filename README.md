# soal-shift-sisop-modul-1-ITB08-2022

### <b> Anggota Kelompok: </b>
##### 1. 5027201001 - Najwa Amelia Qorry 'Aina
##### 2. 5027201024 - Muhammad Ihsanul Afkar
##### 3. 5027201072 - Shafira Khaerunnisa Latif
---

## Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

Catatan:
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

## Penyelesaian soal 1
Untuk kode penuh soal 1, dapat dilihat disini: <br>
[soal1](/soal1/soal1.c) <br>

### 1a
- Nomor 1a mendownload file `music.zip` dan `quote.zip` dengan menggunakan method `wget` dan menyertakan urlnya
```c
    char *wget[] = {"/bin/wget", "wget"};
    char link1[] = "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"; 
    char dir1[] = "/home/najwamel/music.zip";
    char *argv1[] = {wget[1], link1, "-O", dir1, NULL};
    child_p (&cid1, wget[0], argv1);
    
    char link2[] = "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download";
    char dir2[] = "/home/najwamel/quote.zip";
    char *argv2[] = {wget[1], link2, "-O", dir2, NULL};
    child_p (&cid2, wget[0], argv2);
```
- Selanjutnya mengunzip kedua file zip tsb dengan menggunakan `thread` secara bersamaan ke folder `music` untuk music.zip dan folder `quote` untuk quote.zip. Pada code di bawah ini dalam thread memanggil fungsi `unzipbos` yang berfungsi untuk mengunzip filenya.
```c
    pthread_t tid[2];
    int u[5];
    for (int i=0; i<2; i++)
        u[i] = i+1;

    for (int i=0; i<2; i++)
        pthread_create(&(tid[i]), NULL, &unzipbos, (void *)&u[i]);
    
    for (int i=0; i<2; i++)
        pthread_join(tid[i], NULL);
```
- Fungsi `unzipbos` yang dipanggil untuk mengunzip filenya adalah sebagai berikut, dimana `lagubos` akan mengunzip `music.zip` sedangkan `quotebos` akan mengunzip `quote.zip` dan keduanya diunzip ke masing-masing foldernya.
```c
void *unzipbos(void *arg) {
    int *u = (int *)arg;
    int num = *u;
    
    char *lagubos[] = {"unzip", "-qo", "music.zip", "-d", "/home/najwamel/music", NULL};
    char *quotebos[] = {"unzip", "-qo", "quote.zip", "-d", "/home/najwamel/quote", NULL};

    int status;
    pid_t child;
    if (num == 1) {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", lagubos);
        else
            ((wait(&status)) > 0);
    } else {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", quotebos);
        else
            ((wait(&status)) > 0);
    }
}
```
    > Berikut adalah screenshot hasil dari jawaban 1a:

![ss_1a.PNG](images/ss_1a.PNG)

### 1b
- Mendecode file `.txt` dengan `base64`, untuk code kebutuhan decode dapat diakses dari line 201-307 pada [soal1](/soal1/soal1.c)
- Dalam mendecode code utama yaitu pada fungsi `decodebos` sebagai berikut yang akan dipanggil dengan `multithreading` yang ada di `int main` sebagai berikut:
```c
    pthread_t did[2];
    int d[5];
    for (int i=0; i<2; i++)
        d[i] = i+1;
    for (int i=0; i<2; i++)
        pthread_create(&(did[i]), NULL, &decodebos, (void *)&d[i]);
    for (int i=0; i<2; i++)
        pthread_join(did[i], NULL);
```
- Dalam code, akan dilakukan `for looping` untuk mendecode setiap file ada dan diletakkan pada folder yang sesuai dan setiap kalimatnya telah dipisahkan `newline`.
```c
void *decodebos(void *arg) {
    int *d = (int *)arg;
    int num = *d;
    if (num == 1) {
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++) {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            if ((fptr = fopen(path, "r")) == NULL) {
                printf("Error! File cannot be opened.");
                exit(1);
            }
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *pt = decodebos_base64(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", pt);
            fclose(fptr);
        }
        fclose(mout);
    } else {
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++) {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *pt = decodebos_base64(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", pt);
            fclose(fptr);
        }
        fclose(qout);
    }
}
```

### 1c
- Memindahkan file `.txt` hasil decoding ke folder `hasil` dimana dalam `int main` membuat folder `hasil` terlebih dahulu dan kemudian memanggil fungsi `pindahbos` sebagai berikut:
```c
    cid3 = fork();
    if (cid3 < 0) exit(EXIT_FAILURE);
    if (cid3 == 0) {
      char *argv2[] = {"mkdir", "-p", "hasil", NULL};
      execv("/bin/mkdir", argv2);
      exit(EXIT_SUCCESS);
    }
    pindahbos("music.txt");
    pindahbos("quote.txt");
```
- Selanjutnya untuk fungsi `pindahbos` untuk memindah filenya menggunakan method `mv` berisi code sebagai berikut:
```c
void pindahbos(char file[]) {
    int status;
    pid_t child;
    child = fork();
    char bash[] = "mv";
    char target[] = "./hasil";
    if (child == 0)
        execlp(bash, bash, file, target, NULL);
    else
        ((wait(&status)) > 0);
}
```

### 1d
- Meng-zip folder `hasil` dengan password `mihinomenestnajwamel` dengan memanggil fungsi `zippassbos` yang berisi code sebagai berikut:
```c
void zippassbos() {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "mihinomenestnajwamel", "hasil.zip", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
}
```

### 1e
- Meng-unzip `hasil.zip` untuk menambahkan file `no.txt` dan kemudian dipindah ke folder `hasil` lalu dizip berpassword lagi, diimplementasikan pada fungsi `unzipzipbos` yang dipanggil secara `multithreading` pada `int main` sebagai berikut:
```c
    pthread_t fid[2];
    int l[5];
    for (int i=0; i<2; i++)
        l[i] = i+1;
    for (int i=0; i<2; i++)
        pthread_create(&(fid[i]), NULL, &unzipzippass, (void *)&l[i]);  
    for (int i=0; i<2; i++)
        pthread_join(fid[i], NULL);

    pindahbos("no.txt");
    
    zippassbos();
```
- Code di bawah ini merupakan fungsi `unzipzipbos` yang akan mmenjalankan task berupa meng-unzip file `hasil.zip` dan membuat `no.txt`
```c
void *unzipzippass(void *arg) {
    int *a = (int *)arg;
    int num = *a;
    char *hasil[] = {"unzip", "-P", "mihinomenestnajwamel", "-qo", "hasil.zip", "-d", "/home/najwamel/hasil", NULL};
    int status;
    pid_t child;
    
    if (num == 1) {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", hasil);
        else
            ((wait(&status)) > 0);
    } else {
        FILE *add = fopen("no.txt", "w");
        fprintf(add, "No");
        fclose(add);
    }
}
```
    > Berikut adalah screenshot hasil akhir soal1:
- Gambar 1: output hasil decoding pada `music.txt`
![ss_1b.PNG](images/ss_1b.PNG)

- Gambar 2: output hasil decoding pada `quote.txt`
![ss_1c.PNG](images/ss_1c.PNG)

- Gambar 3: output isi dari `hasil.zip`
![ss_1e.PNG](images/ss_1e.PNG)

---

## Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

## Penyelesaian soal 2
Untuk kode penuh soal 2, bisa dilihat disini: <br>
[Client](/soal2/Client/client.c) <br>
[Server](/soal2/Server/server.c) 

Kode kami menggunakan template socket client-server dari [Modul 3](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul3#23-sockets).

Untuk kode client, hanya membuat `while(true)` untuk menerima dan memberi input dan berhenti ketika ada input `skip`.
```c
while (1)
    {
        valread = read(sock, buffer, 1024);
        printf("%s\n", buffer);
        memset(buffer, 0, 1024);
        char input[100];
        scanf("%[^\n]%*c", input);

        send(sock, input, strlen(input), 0);
        if (strcmp(input, "skip") == 0)
        {
            break;
        }
    }
```

### 2a

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file `users.txt` dengan format `username:password`. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut: <br>
- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

Format users.txt:

> username:password <br>
> username2:password2

Berikut adalah kode untuk login maupun register: 
```c
while (1)
    {
        memset(buffer, 0, 1024);
        char msg[100] = "Register or Login (R/L):";
        send(new_socket, msg, strlen(msg), 0);
        valread = read(new_socket, buffer, 1024);

        if (strcmp(buffer, "R") == 0)
        {
            memset(buffer, 0, 1024);
            strcpy(msg, "Enter ID/Username:");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char username[100];
            strcpy(username, buffer);
            memset(buffer, 0, 1024);

            strcpy(msg, "Enter Password");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char password[100];
            strcpy(password, buffer);
            memset(buffer, 0, 1024);
            int status = regist(username, password);
            if (status == -1)
            {
                strcpy(msg, "Username already exist\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else if (status == -2)
            {
                strcpy(msg, "Invalid Password\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else
            {
                strcpy(msg, "Registered successfully\n");
                send(new_socket, msg, strlen(msg), 0);
            }
        }
        else if (strcmp(buffer, "L") == 0)
        {
            memset(buffer, 0, 1024);
            strcpy(msg, "Enter ID/Username:");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char username[100];
            strcpy(username, buffer);
            memset(buffer, 0, 1024);

            strcpy(msg, "Enter Password");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);
            // printf("%s", buffer);
            char password[100];
            strcpy(password, buffer);
            memset(buffer, 0, 1024);
            int status = login(username, password);
            if (status == -1)
            {
                strcpy(msg, "Invalid username or password\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else
            {
                strcpy(msg, "Logged in!\n");
                send(new_socket, msg, strlen(msg), 0);

                // Masuk ke fungsi command...
            }
        }
    }
```
Berikut adalah penjelasan kode:
- `send(new_socket, msg, strlen(msg), 0)` untuk mengirimkan isi `msg` kepada client 
- Terdapat sintaks `if else` untuk mengarahkan input user. Jika R, maka register. Jika L, maka login
- `int status = regist(username, password)`untuk mengetahui status fungsi `regist()` yang fungsinya akan dijelaskan lebih lanjut dibagian bawah ini
- `int status = login(username, password)` sana halnya dengan `regist`, fungsi ini akan dijelaskan lebih lanjut dibawah ini

Untuk fungsi `regist()` dan `login()` adalah sebagai berikut:
```c
int regist(char username[], char password[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("users.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strstr(line, username))
        {
            return -1;
        }
    }
    fclose(fp);
    if (line)
        free(line);

    // Validate password
    int lower_case = 0;
    int upper_case = 0;
    int number_case = 0;
    int length = strlen(password);

    if (length < 6)
        return -2; // Return type password error

    for (int i = 0; i < length; i++)
    {
        if (password[i] >= 'a' && password[i] <= 'z')
            ++lower_case;
        if (password[i] >= 'A' && password[i] <= 'Z')
            ++upper_case;
        if (password[i] >= '0' && password[i] <= '9')
            ++number_case;
    }

    if (lower_case == 0 || upper_case == 0 || number_case == 0)
        return -2;

    // Insert into file
    char result[100];
    strcpy(result, username);
    strcat(result, ":");
    strcat(result, password);

    fp = fopen("users.txt", "a");

    if (fp == NULL)
    {
        printf("error!");
        exit(1);
    }
    fputs(result, fp);
    fputs("\n", fp);
    fclose(fp);

    return 1;
}

int login(char username[], char password[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int mark = 0;
    char account[100];
    fp = fopen("users.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strstr(line, username))
        {
            mark = 1;
            strcpy(account, line);
        }
    }
    fclose(fp);
    if (!mark)
    {
        return -1; // Username not exist
    }

    char user_data[100], pass_data[100];
    char *acc = strtok(account, "\n");
    char *token = strtok(acc, ":");
    strcpy(user_data, token);

    token = strtok(NULL, ":");
    strcpy(pass_data, token);

    if (strcmp(pass_data, password) != 0)
        return -1;

    return 1;
}
```
Penjelasan fungsi `regist()`:
- Fungsi akan membuka file pointer ke file `users.txt` lalu akan mengecek apakah `username` yang di passing sudah ada atau belum dengan `strstr()`. Jika ternyata ditemukan, maka `return -1` yang berarti, username sudah ada.
- Selanjutnya, akan dicek password apakah sesuai ketentuan yang diberikan. Jika tidak, maka `return -2` yang berarti password invalid.
- `username` dan `password` akan digabung sesuai format `username:password` dan akan di-append ke file `users.txt`. Setelah itu,fungsi akan `return 1` yang berarti registrasi sukses.

Penjelasan fungsi `login()`:
- Fungsi akan membuka file `users.txt` dan akan mengecek jika terdapat line yang mengandung `username` dengan `strstr()`. Jika ada, maka salin line tersebut dan beri flag `mark = 1`. Jika tidak ada, maka `return -1` yang berarti `username` atau `password` salah.
- Selanjutnya pisahkan line tersebut dengan delimiter `:` menggunakan fungsi `strtok()`.
- Cek password dari file dengan password dari parameter menggunakan `strcmp()`. Jika tidak sama, maka `return -1` dan jika sama, `return 1` yang berarti login sukses.


### 2b 

Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama `problems.tsv` yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan `\t`. File otomatis dibuat saat server dijalankan. </br>
Untuk kode pembuatan file `problems.tsv` adalah sebagai berikut.
```c
FILE *fp;
fp = fopen("problems.tsv", "a");
fclose(fp);
```

### 2c

Client yang telah login, dapat memasukkan command yaitu `add` yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

Seluruh file akan disimpan oleh server ke dalam folder dengan nama `judul-problem` yang di dalamnya terdapat file `description.txt`, `input.txt` dan `output.txt`. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv. <br>

Berikut adalah kode untuk `add`:
```c
if (strcmp(cmd, "add") == 0)
{
    strcpy(msg, "Title:");
    send(new_socket, msg, strlen(msg), 0);
    valread = read(new_socket, buffer, 1024);
    char title_problem[100];
    strcpy(title_problem, buffer);
    memset(buffer, 0, 1024);

    strcpy(msg, "Filepath description:");
    send(new_socket, msg, strlen(msg), 0);
    valread = read(new_socket, buffer, 1024);
    char description[100];
    strcpy(description, buffer);
    memset(buffer, 0, 1024);

    strcpy(msg, "Filepath input:");
    send(new_socket, msg, strlen(msg), 0);
    valread = read(new_socket, buffer, 1024);
    char input_txt[100];
    strcpy(input_txt, buffer);
    memset(buffer, 0, 1024);

    strcpy(msg, "Filepath output:");
    send(new_socket, msg, strlen(msg), 0);
    valread = read(new_socket, buffer, 1024);
    char out_txt[100];
    strcpy(out_txt, buffer);
    memset(buffer, 0, 1024);

    create_dir(title_problem);

    char description_path[100];
    char input_path[100];
    char output_path[100];

    strcpy(description_path, title_problem);
    strcat(description_path, "/description.txt");
    create_file(description, description_path);

    strcpy(input_path, title_problem);
    strcat(input_path, "/input.txt");
    create_file(input_txt, input_path);

    strcpy(output_path, title_problem);
    strcat(output_path, "/output.txt");
    create_file(out_txt, output_path);

    write_problems(title_problem, username);
}
```
Kode akan mengambil title, description, input, dan output dari client. Setelah itu, akan dibentuk folder sesuai nama title dengan sintaks `create_dir(title_problem)`. Setelah itu akan dibuat file untuk description, input, dan output dari path folder yang sebelumnya sudah dibuat. Setelah itu, title tersebut akan dituliskan kedalam `problems.tsv` menggunakan fungsi `write_problem()`. <br>

Berikut adalah kode fungsi `create_dir()`, `create_file()`, dan `write_problem()`.
```c
void create_dir(char path[])
{
    mkdir(path, S_IRWXU);
}

void create_file(char path[], char dest[])
{
    char ch;

    FILE *source, *target;

    source = fopen(path, "r");

    if (source == NULL)
    {
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");

    if (target == NULL)
    {
        fclose(source);
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void write_problems(char title_problems[], char author[])
{
    FILE *fp;
    char content[100];

    strcpy(content, title_problems);
    strcat(content, "\t");
    strcat(content, author);

    fp = fopen("problems.tsv", "a");
    fputs(content, fp);
    fputs("\n", fp);
    fclose(fp);
}
```

### 2d

Client yang telah login, dapat memasukkan command `see` yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:
- judul-problem-1 by author1
- judul-problem-2 by author2

Untuk kodenya adalah sebagai berikut:
```c
else if (strcmp(cmd, "see") == 0)
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    char data[100];
    char author[100];
    char problem[100];

    fp = fopen("problems.tsv", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
    {
        strcpy(data, line);
        char *temp = strtok(data, "\n");
        char *token = strtok(temp, "\t");
        strcpy(problem, token);
        token = strtok(NULL, "\t");
        strcpy(author, token);

        strcpy(msg, problem);
        strcat(msg, " by ");
        strcat(msg, author);
        strcat(msg, "\n");
        send(new_socket, msg, strlen(msg), 0);
    }
    fclose(fp);
}
```
Kode ini mengambil tiap baris dari `problems.tsv` lalu dipisahkan dengan delimiter `\t`. Setelah itu, di format sehingga membentuk judul by author dan lalu di print.

### 2e

Client yang telah login, dapat memasukkan command `download <judul-problem>` yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu judul-problem. Kedua file tersebut akan disimpan ke folder dengan nama judul-problem di client.

```c
    // If di fungsi main()
    else if (strcmp(cmd, "download") == 0)
    {
        cmd = strtok(NULL, " ");
        char title[100];
        strcpy(title, cmd);
        download(title);
    }

// fungsi download 
void download(char title[])
{
    FILE *fp;
    char desc_path[100] = "/home/kali/sisop/modul-3/Client/description.txt";
    char input_path[100] = "/home/kali/sisop/modul-3/Client/input.txt";
    char server_path[100];

    strcpy(server_path, title);
    strcat(server_path, "/");
    strcat(server_path, "description.txt");
    create_file(server_path, desc_path);

    strcpy(server_path, title);
    strcat(server_path, "/");
    strcat(server_path, "input.txt");
    create_file(server_path, input_path);
}
```
Pada fungsi `download()`, terdapat parameter `title` yang berupa input dari client. Disini, client akan di define path-nya. Setelah itu, akan di buat file dengan isi yang sama dari `<title>/decription.txt` dan `<title>/input.txt` ke path client.

### 2f

Client yang telah login, dapat memasukkan command `submit <judul-problem> <path-file-output.txt>`.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file `output.txt` nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file `output.txt` yang telah dikirimkan oleh client dan `output.txt` yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan **AC** dan jika tidak maka server akan mengeluarkan pesan **WA**.

Untuk kode adalah sebagai berikut:
```c
int main(){
    // ............
    else if (strcmp(cmd, "submit") == 0)
    {
        cmd = strtok(NULL, " ");
        char title_path[100], file_path[100];
        strcpy(title_path, cmd);
        strcat(title_path, "/output.txt");

        cmd = strtok(NULL, " ");
        strcpy(file_path, cmd);

        int ans = check_file(title_path, file_path);
        if (ans == 1)
        {
            strcpy(msg, "AC\n");
            send(new_socket, msg, strlen(msg), 0);
        }
        else
        {
            strcpy(msg, "WA\n");
            send(new_socket, msg, strlen(msg), 0);
        }
    }
    // ..............
}
// Fungsi check_file
int check_file(char source[], char answer[])
{
    FILE *file1 = fopen(source, "r");
    FILE *file2 = fopen(answer, "r");
    char ch1 = getc(file1);
    char ch2 = getc(file2);
    int error = 0;
    while (ch1 != EOF && ch2 != EOF)
    {
        if (ch1 != ch2)
        {
            error = 1;
        }
        ch1 = getc(file1);
        ch2 = getc(file2);
    }
    fclose(file1);
    fclose(file2);
    if (error != 0)
        return 0;
    else
        return 1;
}
```
Di bagian submit di `main()`, hasil input command client dipisah menggunakan `strtok()` untuk mendapatkan `title` dan `output.txt` sehingga dapat dibandingkan dengan fungsi `check_file()`. Di fungsi tersebut, akan menerima 2 parameter yauto path file source dan answer dimana source merupakan `output.txt` yang ada di server, sedangkan answer merupakan file path dari client. Kedua file tersebut akan dibandingkan, jika kedua file sama, maka kirim `AC` ke client, dan jika tidak, kirim `WA`.

### 2g

Untuk poin ini, kami belum bisa menemukan caranya sehingga poin ini kami skip.

## Soal 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang,
Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan
jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan,
Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

Contoh jika program pengkategorian dijalankan
```
# Program soal3 terletak di /home/[user]/shift3/hartakarun
$ ./soal3
# Hasilnya adalah sebagai berikut
/home/[user]/shift3/hartakarun
|-jpg
|--file1.jpg
|--file2.jpg
|--file3.jpg
|-c
|--file1.c
|-tar.gz
|--file1.tar.gz

```
Catatan:
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik
terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system()


## Penyelesaian soal 3

Untuk program pada soal 3, dapat dilihat disini: <br>
[soal3](/soal3/soal3.c) <br>

### 3a-3c
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan
ke dalam folder `/home/[user]/shift3/`. Kemudian working directory program akan
berada pada folder `/home/[user]/shift3/hartakarun/`. Karena Nami tidak ingin ada
file yang tertinggal, program harus mengkategorikan seluruh file pada working
directory secara rekursif.
- Pertama-tama, program akan mendeklarasikan `gandafile` dan nama `direktori` yang akan dikategorikan berdasarkan ekstensinya nanti, kemudian akan dilakukan iterasi pembuatan directory baru menggunakan array dengan looping do while sampai lebih kecil dari 14
```c
pthread_t tid[500];
char gandafile[2000][2000];
const char *dir[] = {"jpg", "tar.gz", "c", "png", "txt", "zip", "gif", "pdf", "jpeg", "sh", "hex", "bin", "Unknown", "Hidden", "js", "gns3project", "fbx", };

int file(void) {
    int i = 0;
    do{
       mkdir(dir[i], S_IRWXU);
       i++;
    }
    while (i<14);
}
```
- Perintah S_IRWXU digunakan untuk membaca, menulis, dan mengeksekusi izin yang ada pada looping tersebut.

Kemudian masuk ke fungsi utama yaitu `checkType`  untuk mengecek tipe file/directory dan berguna untuk mengkategorikan file
```c
int checkType(const char *path){
  struct stat path_stat;
  stat(path, &path_stat);
  return S_ISREG(path_stat.st_mode);
}
```
- Terdapat `struct stat` yang berisi beberapa elemen salah satunya `st_mode` untuk mengecek apakah file merupakan tipe biasa atau bukan folder.

Kemudian fungsi `rekursive` untuk mengkategorikan seluruh file pada directory secara rekursif
```c
void rekursive(char *basePath, int *iter)
{
    // int iter = 0;
  char path[225];
  struct dirent *display;
  DIR *dir = opendir(basePath);

  while ((display = readdir(dir)) != NULL)
  {
    if (strcmp(display->d_name, ".") != 0 && strcmp(display->d_name, "..") != 0)
      {
        strcpy(path, basePath), strcat(path, "/"),strcat(path, display->d_name) ;

        if(checkType(path)){
          strcpy(gandafile[*iter], path);
          *iter = *iter + 1;
        }
        else{
          rekursive(path, iter);
        }
      }
  }
    closedir(dir);
}
```
- Untuk memasukkan setiap path ke dalam thread masing-masing, maka setiap path file akan disimpan ke dalam suatu array untuk digunakan lagi di dalam fungsi `moveFile`
- Pada syntax `if(checkType(path))` berguna jika path/direktori masih berupa folder, maka akan terus melakukan rekursif hingga menemukan list path yang file biasa.

Kemudian pada fungsi `moveFile ` berguna untuk memindahkan file-file ke dalam folder berdasarkan ekstensinya
```c
void *moveFile(void *arg){
  char str[225], temp[225], temp2[225], temp3[225], temp4[225], path[225], tempDest[225], cwd[225], fileName[225], fileExt[225], fileExt2[225];

  int checkFile = checkType(path);
  getcwd(cwd, sizeof(cwd)), strcpy(path, (char*) arg), strcpy(temp4, path);
  
  char *fileExt3, dot1 = '.';

  fileExt3 = strchr(temp4, dot1);
  strcpy(temp, path);

  char *token=strtok(temp, "/");
  do{
       sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }
  while(token != NULL);

  strcpy(temp, path), strcpy(temp2, fileName), strcpy(temp3, fileName);

  int total = 0;
  char *token2=strtok(temp2, ".");

  sprintf(fileExt2, "%s", token2);
    do
    {
        total++;

      sprintf(fileExt, "%s", token2);

      token2=strtok(NULL, ".");
    }

    while(token2 != NULL);

  char dot = '.', first = temp3[0];
  if(dot == first) 
    {
     strcpy(fileExt, "Hidden");
    }

  else if(total>=3)
      {
        strcpy(fileExt, fileExt3+1);
      }

  else if(total<=1 )
      {
        strcpy(fileExt, "Unknown");
      }

  int i = 0;
  do
  {
    fileExt[i] = tolower(fileExt[i]);
    i++;
  }
  while(i<sizeof(fileExt));
```
- Untuk mendapatkan ekstensi yang akan digunakan sebagai nama folder, maka dapat menggunakan `strchr` untuk mengembalikan nilai `.` pertama yang ditemukan akan langsung ditiadakan. String `strchr()`digunakan untuk mencari kemunculan pertama dari sebuah string di dalam string lain
- Untuk mendapatkan nama-nama file menggunakan `strtok` dan `looping` untuk memotong string dan menghilangkan delimiter. String `strtok` merupakan perintah dalam `library string.h` yang berfungsi untuk memisahkan string dari karakter tertentu.
- Kemudian menghitung berapa banyak string yang sudah dipisah dan berapa banyak kemunculan `.`  pada nama file serta mencari nama file tanpa adanya ekstensi menggunakan `strtok` dan `looping` serta menggunakan count untuk menghitung kemunculan `.`

Kemudian mengecek untuk pengkategorian file
```c
if(dot == first) 
  {
   strcpy(fileExt, "Hidden");
  }

else if(total >= 3)
    {
      strcpy(fileExt, fileExt3+1);
    }

else if (total <=1 )
    {
      strcpy(fileExt, "Unknown");
    }
```
- `if(dot==first)`  untuk mengkategorikan file-file hidden agar bisa masuk ke folder `hidden`
-  `else if(total>=3)` apabila kemunculan `.` lebih dari 1 maka akan masuk ke folder dengan titik terdepan
- `else if (total <=1)` apabila tidak ada ekstensi (count kurang dari 1), sehingga fileExt akan menyimpan `unknown` sehingga akan dibuat folder `unknown` untuk file-file tanpa ekstensi.

Kemudian mengubah ekstensi menjadi case yang tidak sensitif
```c
int i = 0;
do
{
  fileExt[i] = tolower(fileExt[i]);
  i++;
}
while(i < sizeof(fileExt));
```
Kemudian membuat folder dengan `mkdir`
```c
strcpy(temporary, (char*) arg);
mkdir(fileExt, 0777);
strcat(cwd, "/"), strcat(cwd,fileExt),  strcat(cwd, "/"), strcat(cwd, namafile), strcpy(tempDest, cwd), rename(temporary, tempDest);
return (void *) 1;
}
```
- Pada bagian ini berguna untuk melakukan pemindahan file dari path lama ke path yang baru.


> Berikut adalah screenshot mengkategorikan seluruh file pada directory secara rekursif:

![3a.png](images/3a.PNG)

![3c.png](images/3c.PNG)


> Berikut adalah screenshot jika terdapat file yang tidak memiliki ekstensi:

![3b.png](images/3b.PNG)


### 3d-3e

### Kendala
- Terdapat beberapa file yang tidak masuk ke dalam folder berdasarkan format ekstensi folder
- Pada soal 3d-3e terdapat kendala karena ngestak dan bingung harus diapakan:)


