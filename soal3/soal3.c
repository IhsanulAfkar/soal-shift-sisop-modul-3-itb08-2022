#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>
#include <ctype.h>

pthread_t tid[500];
char gandafile[2000][2000];
const char *dir[] = {"jpg", "tar.gz", "c", "png", "txt", "zip", "gif", "pdf", "jpeg", "sh", "hex", "bin", "Unknown", "Hidden", "js", "gns3project", "fbx", };

int file(void) {
    int i = 0;
    do{
       mkdir(dir[i], S_IRWXU);
       i++;
    }
    while (i<14);
}

int checkType(const char *path){
  struct stat path_stat;
  stat(path, &path_stat);
  return S_ISREG(path_stat.st_mode);
}

void rekursive(char *basePath, int *iter)
{
    // int iter = 0;
  char path[225];
  struct dirent *display;
  DIR *dir = opendir(basePath);

  while ((display = readdir(dir)) != NULL)
  {
    if (strcmp(display->d_name, ".") != 0 && strcmp(display->d_name, "..") != 0)
      {
        strcpy(path, basePath), strcat(path, "/"),strcat(path, display->d_name) ;

        if(checkType(path)){
          strcpy(gandafile[*iter], path);
          *iter = *iter + 1;
        }
        else{
          rekursive(path, iter);
        }
      }
  }
    closedir(dir);
}

void *moveFile(void *arg){
  char str[225], temp[225], temp2[225], temp3[225], temp4[225], path[225], tempDest[225], cwd[225], fileName[225], fileExt[225], fileExt2[225];

  int checkFile = checkType(path);
  getcwd(cwd, sizeof(cwd)), strcpy(path, (char*) arg), strcpy(temp4, path);
  
  char *fileExt3, dot1 = '.';

  fileExt3 = strchr(temp4, dot1);
  strcpy(temp, path);

  char *token=strtok(temp, "/");
  do{
       sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }
  while(token != NULL);

  strcpy(temp, path), strcpy(temp2, fileName), strcpy(temp3, fileName);

  int total = 0;
  char *token2=strtok(temp2, ".");

  sprintf(fileExt2, "%s", token2);
    do
    {
        total++;

      sprintf(fileExt, "%s", token2);

      token2=strtok(NULL, ".");
    }

    while(token2 != NULL);

  char dot = '.', first = temp3[0];
  if(dot == first) 
    {
     strcpy(fileExt, "Hidden");
    }

  else if(total>=3)
      {
        strcpy(fileExt, fileExt3+1);
      }

  else if(total<=1 )
      {
        strcpy(fileExt, "Unknown");
      }

  int i = 0;
  do
  {
    fileExt[i] = tolower(fileExt[i]);
    i++;
  }
  while(i<sizeof(fileExt));

  strcpy(temp, (char*) arg);
  mkdir(fileExt, 0777);
  strcat(cwd, "/"), strcat(cwd,fileExt),  strcat(cwd, "/"), strcat(cwd, fileName), strcpy(tempDest, cwd), rename(temp, tempDest);
  return (void *) 1;
}

int main(int argc, char *argv[]) {
  void *status;
  char basePath[1000], cwd[1000];
  int iter = 0;
    getcwd(cwd, sizeof(cwd)),  strcpy(basePath, cwd), rekursive(basePath, &iter);
    pthread_t tid[iter];
      int i = 0;
      do
      {
          pthread_create(&tid[i], NULL, moveFile, (void*)gandafile[i]);
          pthread_join(tid[i], &status);
          i++;
      }
      while (i<iter);
      exit(EXIT_FAILURE);

}

    

