#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

int regist(char username[], char password[]);
int login(char username[], char password[]);
void create_dir(char path[]);
void create_file(char path[], char dest[]);
void write_problems(char title_problems[], char author[]);
void see();
void download(char title[]);
int check_file(char source[], char answer[]);

int main(int argc, char const *argv[])
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    FILE *fp;
    fp = fopen("problems.tsv", "a");
    fclose(fp);
    while (1)
    {
        memset(buffer, 0, 1024);
        char msg[100] = "Register or Login (R/L):";
        send(new_socket, msg, strlen(msg), 0);
        valread = read(new_socket, buffer, 1024);

        if (strcmp(buffer, "R") == 0)
        {
            memset(buffer, 0, 1024);
            strcpy(msg, "Enter ID/Username:");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char username[100];
            strcpy(username, buffer);
            memset(buffer, 0, 1024);

            strcpy(msg, "Enter Password");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char password[100];
            strcpy(password, buffer);
            memset(buffer, 0, 1024);
            int status = regist(username, password);
            if (status == -1)
            {
                strcpy(msg, "Username already exist\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else if (status == -2)
            {
                strcpy(msg, "Invalid Password\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else
            {
                strcpy(msg, "Registered successfully\n");
                send(new_socket, msg, strlen(msg), 0);
            }
        }
        else if (strcmp(buffer, "L") == 0)
        {
            memset(buffer, 0, 1024);
            strcpy(msg, "Enter ID/Username:");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);

            char username[100];
            strcpy(username, buffer);
            memset(buffer, 0, 1024);

            strcpy(msg, "Enter Password");
            send(new_socket, msg, strlen(msg), 0);
            valread = read(new_socket, buffer, 1024);
            // printf("%s", buffer);
            char password[100];
            strcpy(password, buffer);
            memset(buffer, 0, 1024);
            int status = login(username, password);
            if (status == -1)
            {
                strcpy(msg, "Invalid username or password\n");
                send(new_socket, msg, strlen(msg), 0);
            }
            else
            {
                strcpy(msg, "Logged in!\n");
                send(new_socket, msg, strlen(msg), 0);
                while (1)
                {
                    strcpy(msg, "Please type a command:");
                    send(new_socket, msg, strlen(msg), 0);
                    valread = read(new_socket, buffer, 1024);
                    char command[100];
                    char text_path[100];
                    strcpy(command, buffer);
                    memset(buffer, 0, 1024);

                    char *cmd = strtok(command, " ");
                    printf("test");
                    if (strcmp(cmd, "add") == 0)
                    {
                        strcpy(msg, "Title:");
                        send(new_socket, msg, strlen(msg), 0);
                        valread = read(new_socket, buffer, 1024);
                        char title_problem[100];
                        strcpy(title_problem, buffer);
                        memset(buffer, 0, 1024);

                        strcpy(msg, "Filepath description:");
                        send(new_socket, msg, strlen(msg), 0);
                        valread = read(new_socket, buffer, 1024);
                        char description[100];
                        strcpy(description, buffer);
                        memset(buffer, 0, 1024);

                        strcpy(msg, "Filepath input:");
                        send(new_socket, msg, strlen(msg), 0);
                        valread = read(new_socket, buffer, 1024);
                        char input_txt[100];
                        strcpy(input_txt, buffer);
                        memset(buffer, 0, 1024);

                        strcpy(msg, "Filepath output:");
                        send(new_socket, msg, strlen(msg), 0);
                        valread = read(new_socket, buffer, 1024);
                        char out_txt[100];
                        strcpy(out_txt, buffer);
                        memset(buffer, 0, 1024);

                        create_dir(title_problem);

                        char description_path[100];
                        char input_path[100];
                        char output_path[100];

                        strcpy(description_path, title_problem);
                        strcat(description_path, "/description.txt");
                        create_file(description, description_path);

                        strcpy(input_path, title_problem);
                        strcat(input_path, "/input.txt");
                        create_file(input_txt, input_path);

                        strcpy(output_path, title_problem);
                        strcat(output_path, "/output.txt");
                        create_file(out_txt, output_path);

                        write_problems(title_problem, username);
                    }
                    else if (strcmp(cmd, "see") == 0)
                    {
                        FILE *fp;
                        char *line = NULL;
                        size_t len = 0;
                        ssize_t read;
                        char data[100];
                        char author[100];
                        char problem[100];

                        fp = fopen("problems.tsv", "r");
                        if (fp == NULL)
                            exit(EXIT_FAILURE);

                        while ((read = getline(&line, &len, fp)) != -1)
                        {
                            strcpy(data, line);
                            char *temp = strtok(data, "\n");
                            char *token = strtok(temp, "\t");
                            strcpy(problem, token);
                            token = strtok(NULL, "\t");
                            strcpy(author, token);

                            strcpy(msg, problem);
                            strcat(msg, " by ");
                            strcat(msg, author);
                            strcat(msg, "\n");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                        fclose(fp);
                    }
                    else if (strcmp(cmd, "download") == 0)
                    {
                        cmd = strtok(NULL, " ");
                        char title[100];
                        strcpy(title, cmd);
                        download(title);
                    }
                    else if (strcmp(cmd, "submit") == 0)
                    {
                        cmd = strtok(NULL, " ");
                        char title_path[100], file_path[100];
                        strcpy(title_path, cmd);
                        strcat(title_path, "/output.txt");

                        cmd = strtok(NULL, " ");
                        strcpy(file_path, cmd);

                        int ans = check_file(title_path, file_path);
                        if (ans == 1)
                        {
                            strcpy(msg, "AC\n");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                        else
                        {
                            strcpy(msg, "WA\n");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                    }
                    else if (strcmp(cmd, "skip") == 0)
                    {
                        break;
                    }
                }
            }
        }
    }
    return 0;
}

void create_dir(char path[])
{
    mkdir(path, S_IRWXU);
}

int check_file(char source[], char answer[])
{
    FILE *file1 = fopen(source, "r");
    FILE *file2 = fopen(answer, "r");
    char ch1 = getc(file1);
    char ch2 = getc(file2);
    int error = 0;
    while (ch1 != EOF && ch2 != EOF)
    {
        if (ch1 != ch2)
        {
            error = 1;
        }
        ch1 = getc(file1);
        ch2 = getc(file2);
    }
    fclose(file1);
    fclose(file2);
    if (error != 0)
        return 0;
    else
        return 1;
}

void create_file(char path[], char dest[])
{
    char ch;

    FILE *source, *target;

    source = fopen(path, "r");

    if (source == NULL)
    {
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");

    if (target == NULL)
    {
        fclose(source);
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void write_problems(char title_problems[], char author[])
{
    FILE *fp;
    char content[100];

    strcpy(content, title_problems);
    strcat(content, "\t");
    strcat(content, author);

    fp = fopen("problems.tsv", "a");
    fputs(content, fp);
    fputs("\n", fp);
    fclose(fp);
}

void see()
{
}

int regist(char username[], char password[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("users.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strstr(line, username))
        {
            return -1;
        }
    }
    fclose(fp);
    if (line)
        free(line);

    // Validate password
    int lower_case = 0;
    int upper_case = 0;
    int number_case = 0;
    int length = strlen(password);

    if (length < 6)
        return -2; // Return type password error

    for (int i = 0; i < length; i++)
    {
        if (password[i] >= 'a' && password[i] <= 'z')
            ++lower_case;
        if (password[i] >= 'A' && password[i] <= 'Z')
            ++upper_case;
        if (password[i] >= '0' && password[i] <= '9')
            ++number_case;
    }

    if (lower_case == 0 || upper_case == 0 || number_case == 0)
        return -2;

    // Insert into file
    char result[100];
    strcpy(result, username);
    strcat(result, ":");
    strcat(result, password);

    fp = fopen("users.txt", "a");

    if (fp == NULL)
    {
        printf("error!");
        exit(1);
    }
    fputs(result, fp);
    fputs("\n", fp);
    fclose(fp);

    return 1;
}

int login(char username[], char password[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int mark = 0;
    char account[100];
    fp = fopen("users.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strstr(line, username))
        {
            mark = 1;
            strcpy(account, line);
        }
    }
    fclose(fp);
    if (!mark)
    {
        return -1; // Username not exist
    }

    char user_data[100], pass_data[100];
    char *acc = strtok(account, "\n");
    char *token = strtok(acc, ":");
    strcpy(user_data, token);

    token = strtok(NULL, ":");
    strcpy(pass_data, token);

    if (strcmp(pass_data, password) != 0)
        return -1;

    return 1;
}

void download(char title[])
{
    FILE *fp;
    char desc_path[100] = "/home/kali/sisop/modul-3/Client/description.txt";
    char input_path[100] = "/home/kali/sisop/modul-3/Client/input.txt";
    char server_path[100];

    strcpy(server_path, title);
    strcat(server_path, "/");
    strcat(server_path, "description.txt");
    create_file(server_path, desc_path);

    strcpy(server_path, title);
    strcat(server_path, "/");
    strcat(server_path, "input.txt");
    create_file(server_path, input_path);
}