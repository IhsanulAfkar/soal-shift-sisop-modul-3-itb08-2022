#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include <stdint.h>
#include <dirent.h>
#include <wait.h>

// fungsi prototype
void child_p (pid_t *cid, char *arg, char **argv);
void *unzipbos(void *arg);
void *decodebos(void *arg);
void pindahbos(char file[]); 
void zippassbos();
void *unzipzippass(void *arg);
void table_decodebos();
void clearbos_base64();
char *encodebos_base64(const unsigned char *data, size_t input_length, size_t *output_length);
unsigned char *decodebos_base64(const char *data, size_t input_length, size_t *output_length);

// inisiasi decode base64
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

// main fungsi
int main() {
    pid_t cid, cid1, cid2, cid3;
    int status;

    //1a membuat folder quote dan music
    cid1 = fork();
    if (cid1 < 0) exit(EXIT_FAILURE);
    if (cid1 == 0) {
      char *argv1[] = {"mkdir", "-p", "quote", NULL};
      execv("/bin/mkdir", argv1);
      exit(EXIT_SUCCESS);
    } 
    
    cid2 = fork();
    if (cid2 < 0) exit(EXIT_FAILURE);
    if (cid2 == 0) {
      char *argv2[] = {"mkdir", "-p", "music", NULL};
      execv("/bin/mkdir", argv2);
      exit(EXIT_SUCCESS);
    } 
    
    //1a download music.zip dan quote.zip
    char *wget[] = {"/bin/wget", "wget"};
    char link1[] = "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"; 
    char dir1[] = "/home/najwamel/music.zip";
    char *argv1[] = {wget[1], link1, "-O", dir1, NULL};
    child_p (&cid1, wget[0], argv1);
    
    char link2[] = "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download";
    char dir2[] = "/home/najwamel/quote.zip";
    char *argv2[] = {wget[1], link2, "-O", dir2, NULL};
    child_p (&cid2, wget[0], argv2);
    
    //1a meng-unzip quote.zip dan music.zip ke folder masing-masing
    pthread_t tid[2];
    int u[5];
    for (int i=0; i<2; i++)
        u[i] = i+1;

    for (int i=0; i<2; i++)
        pthread_create(&(tid[i]), NULL, &unzipbos, (void *)&u[i]);
    
    for (int i=0; i<2; i++)
        pthread_join(tid[i], NULL);
        
    //1b decode .txt ke music.txt dan quote.txt
    pthread_t did[2];
    int d[5];
    for (int i=0; i<2; i++)
        d[i] = i+1;
    for (int i=0; i<2; i++)
        pthread_create(&(did[i]), NULL, &decodebos, (void *)&d[i]);
    for (int i=0; i<2; i++)
        pthread_join(did[i], NULL);
        
    //1c pindah hasil decode ke folder hasil
    cid3 = fork();
    if (cid3 < 0) exit(EXIT_FAILURE);
    if (cid3 == 0) {
      char *argv2[] = {"mkdir", "-p", "hasil", NULL};
      execv("/bin/mkdir", argv2);
      exit(EXIT_SUCCESS);
    }
    pindahbos("music.txt");
    pindahbos("quote.txt");
    
    //1d zip menjadi hasil.zip dan dipassword
    zippassbos();
    
    //1e unzip, buat no.txt, zip pass lagi hasilnya
    pthread_t fid[2];
    int l[5];
    for (int i=0; i<2; i++)
        l[i] = i+1;
    for (int i=0; i<2; i++)
        pthread_create(&(fid[i]), NULL, &unzipzippass, (void *)&l[i]);  
    for (int i=0; i<2; i++)
        pthread_join(fid[i], NULL);

    pindahbos("no.txt");
    
    zippassbos();

    return 0;
}

void child_p (pid_t *cid, char *arg, char **argv) {
    *cid = fork();
    if (*cid < 0) exit(1);
    if (*cid == 0) {
        execv(arg, argv);
        exit(0);
    }
    return;
}

// unzip ke dalam folder
void *unzipbos(void *arg) {
    int *u = (int *)arg;
    int num = *u;
    
    char *lagubos[] = {"unzip", "-qo", "music.zip", "-d", "/home/najwamel/music", NULL};
    char *quotebos[] = {"unzip", "-qo", "quote.zip", "-d", "/home/najwamel/quote", NULL};

    int status;
    pid_t child;
    if (num == 1) {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", lagubos);
        else
            ((wait(&status)) > 0);
    } else {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", quotebos);
        else
            ((wait(&status)) > 0);
    }
}

void pindahbos(char file[]) {
    int status;
    pid_t child;
    child = fork();
    char bash[] = "mv";
    char target[] = "./hasil";
    if (child == 0)
        execlp(bash, bash, file, target, NULL);
    else
        ((wait(&status)) > 0);
}

// zip dengan passsword    
void zippassbos() {
    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "mihinomenestnajwamel", "hasil.zip", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
}

// unzip, buat no.txt, zip pass lagi hasilnya
void *unzipzippass(void *arg) {
    int *a = (int *)arg;
    int num = *a;
    char *hasil[] = {"unzip", "-P", "mihinomenestnajwamel", "-qo", "hasil.zip", "-d", "/home/najwamel/hasil", NULL};
    int status;
    pid_t child;
    
    if (num == 1) {
        child = fork();
        if (child == 0)
            execv("/usr/bin/unzip", hasil);
        else
            ((wait(&status)) > 0);
    } else {
        FILE *add = fopen("no.txt", "w");
        fprintf(add, "No");
        fclose(add);
    }
}

// fungsi-fungsi untuk mendecode dengan base64
void *decodebos(void *arg) {
    int *d = (int *)arg;
    int num = *d;
    if (num == 1) {
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++) {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            if ((fptr = fopen(path, "r")) == NULL) {
                printf("Error! File cannot be opened.");
                exit(1);
            }
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *pt = decodebos_base64(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", pt);
            fclose(fptr);
        }
        fclose(mout);
    } else {
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++) {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *pt = decodebos_base64(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", pt);
            fclose(fptr);
        }
        fclose(qout);
    }
}

void table_decodebos() {
    decoding_table = malloc(256);
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void clearbos_base64() {
    free(decoding_table);
}

char *encodebos_base64(const unsigned char *data, size_t input_length, size_t *output_length) {
    *output_length = 4 * ((input_length + 2) / 3);
    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;
    for (int i = 0, j = 0; i < input_length;) {
        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }
    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *decodebos_base64(const char *data, size_t input_length, size_t *output_length) {
    if (decoding_table == NULL)
        table_decodebos();
    if (input_length % 4 != 0)
        return NULL;
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;
    for (int i = 0, j = 0; i < input_length;) {
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}
